import stim
import collections
import math
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as plt
import matplotlib.pyplot as pltp
import collections
import scipy









#New idea by Liberto from 21.11.23
#Do feedback on the data qbs only in the first and last cycle. Optionally do Ancilla feedback and compare

def bell_universal_4(p_eg = 0.0,
                     p_ge = 0.01, 
                     cycles = 10,
                     phase = 0, 
                     parity = 0, 
                     ancilla_fb = False, 
                     ancilla_fb_adaptive = False,
                     rounds = 1,
                     runs = 1000,
                     after_reset_flip_probability=0.001, 
                     after_clifford_depolarization_1= 0.001, 
                     after_clifford_depolarization_2= 0.01, 
                     before_measure_flip_probability= 0.001, 
                     before_round_data_depolarization=0.001,
                     break_experiment = False,
                     ancilla_multiple = False,
                     mixed_model = False, 
                     ancilla_reset = True,
                     lut_enable = False):

    """

    arguments:

    cycles: How often measure ZZ and XX stabilizers
    p_ge: Decoherene probability from 1 to 0 state
    p_eg: Decoherene probability from 0 to 1 state
    parity: In which parity to stabilize the state
    phase: In which phase to stabilize the state
    ancilla_fb: If the true apply X gate before ancilla measurment if expected value is 1
    state_switch: Option to stabilize phi00 and switch to phi11 in last cycle
    rounds: Based on how many rounds one does feedback
    runs: How many experiments
    break_experiment: 
    ancilla_reset: Indicate whether we reset the ancilla to 0 after each msmt
    lut_enable: If rounds >= 3, decode msmts via a LUT

    
    """
    errors = 0
    count = 0
    ancilla_idx = 1
    data_0_idx = 0
    data_1_idx = 2
    
    for l in range(runs):
        sim = stim.TableauSimulator()

         #FIFO queue of measurments
        parity_meas = collections.deque(maxlen = rounds)
        phase_meas = collections.deque(maxlen = rounds)              

        flip = False
        parity_last = parity
        phase_last = phase
        break_loop = False
        last_ms = False
        last_ms_helper = False #Use this variable just as a helper to store last_ms value

        #Initialisation circuit
        sim.reset(data_0_idx)
        sim.reset(ancilla_idx)
        sim.reset(data_1_idx)
        sim.x_error(data_0_idx, p = after_reset_flip_probability)                
        sim.x_error(ancilla_idx, p = after_reset_flip_probability) 
        sim.x_error(data_1_idx, p = after_reset_flip_probability)

        sim.h(data_0_idx)
        sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
        sim.h(data_1_idx)    
        sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)       

        #If we want to perform ancilla fb we need one cycle to reach a fidelity
        #at which it makes sense to predict the ancillas state


                
        #ZZ, no ancilla feedback
        sim.cnot(data_0_idx,ancilla_idx)                                               
        sim.cnot(data_1_idx,ancilla_idx)
        sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
        sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)
                    
        anc = sim.peek_bloch(ancilla_idx)
        if anc == stim.PauliString("-Z"):
            sim.x_error(ancilla_idx, p = p_ge)

        #Symmetric Readout error
        sim.x_error(ancilla_idx, p = before_measure_flip_probability)    
        parity_last = sim.measure(ancilla_idx)

        sim.reset(ancilla_idx)
        sim.x_error(ancilla_idx, p = after_reset_flip_probability)

        if parity_last == 1 - parity:
            sim.x(data_1_idx)
            parity_last = parity
            


        #XX, no ancilla feedback 
        sim.h(data_0_idx)                                                        
        sim.h(data_1_idx)
        sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
        sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)
        
        sim.cnot(data_0_idx,ancilla_idx)
        sim.cnot(data_1_idx,ancilla_idx)
        sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
        sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)

        anc = sim.peek_bloch(1)
        if anc == stim.PauliString("-Z"):
            sim.x_error(ancilla_idx, p = p_ge)

        sim.x_error(ancilla_idx, p = before_measure_flip_probability)
        phase_last = sim.measure(ancilla_idx) 
        
        sim.reset(ancilla_idx)
        sim.x_error(ancilla_idx, p = after_reset_flip_probability)

        if phase_last == 1 - phase:
            sim.x(data_1_idx)
            phase_last = phase

        sim.h(data_0_idx)
        sim.h(data_1_idx)
        sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
        sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)


        #cycles with feedback only on the ancilla qb, not on data qbs
        for j in range(cycles):
            #ZZ 
            sim.cnot(data_0_idx,ancilla_idx)                                               
            sim.cnot(data_1_idx,ancilla_idx)
            sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
            sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)

            if ancilla_fb:                                         
                if(ancilla_fb_adaptive and ancilla_multiple == False):                                
                    if parity_last:                                     
                        sim.x(ancilla_idx)                                        
                        flip = True

                elif(ancilla_fb_adaptive and ancilla_multiple == True):
                    if(len(parity_meas) == rounds):
                            # if sum([int(parity) for parity in parity_meas]) >= rounds - 1:
                        if list(parity_meas) in [ [True, True, True],[True, False, True], [False, True, True]] : 
                                sim.x(ancilla_idx)                                        
                                flip = True
     
                    # elif ancilla_fb_adaptive == False:                      
                    #     if parity:                                           
                    #         sim.x(ancilla_idx)                                        
                    #         flip = True
                            
            anc = sim.peek_bloch(ancilla_idx)
            if anc == stim.PauliString("-Z"):
                sim.x_error(ancilla_idx, p = p_ge)
    

            #Symmetric Readout error
            sim.x_error(ancilla_idx, p = before_measure_flip_probability)    
            parity_last = sim.measure(ancilla_idx)
        
            sim.reset(ancilla_idx)
            sim.x_error(ancilla_idx, p = after_reset_flip_probability)
        
    

            #If we performed ancilla fb we need to invert our msmt result
            if ancilla_fb:                                              
                if flip:                                             
                    parity_last = 1 - parity_last
                    flip = False
            
            parity_meas.append(parity_last)



            #XX 
            sim.h(data_0_idx)                                                        
            sim.h(data_1_idx)
            sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
            sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)
            
            sim.cnot(data_0_idx,ancilla_idx)
            sim.cnot(data_1_idx,ancilla_idx)
            sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
            sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)

            if ancilla_fb:                                         
                if(ancilla_fb_adaptive and ancilla_multiple == False):                               
                    if phase_last:                                     
                        sim.x(ancilla_idx)                                        
                        flip = True
                
                elif(ancilla_fb_adaptive and ancilla_multiple == True):
                    if(len(phase_meas) == rounds):
                        # if sum([int(phase) for phase in phase_meas]) >= rounds - 1:
                        if list(phase_meas) in [ [True, True, True],[True, False, True], [False, True, True]] : 
                            sim.x(ancilla_idx)                                        
                            flip = True
                    
                # elif ancilla_fb_adaptive == False:                      
                #     if phase:                                          
                #         sim.x(ancilla_idx)                                        
                #         flip = True
            
            anc = sim.peek_bloch(ancilla_idx)
            if anc == stim.PauliString("-Z"):
                sim.x_error(ancilla_idx, p = p_ge)

            sim.x_error(ancilla_idx, p = before_measure_flip_probability)
            phase_last = sim.measure(ancilla_idx)
            
            sim.reset(ancilla_idx)
            sim.x_error(ancilla_idx, p = after_reset_flip_probability)
            
                
            if ancilla_fb:
                if flip:
                    phase_last = 1 - phase_last
                    flip = False

            phase_meas.append(phase_last)
        
            sim.h(data_0_idx)
            sim.h(data_1_idx)
            sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
            sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)
        

        #Last cycle in which we perform the actual feedback on the data qubits
        #ZZ

        # if(lut_enable and not mixed_model):
        #     if parity:
        #         if sum([int(parity) for parity in parity_meas]) <= 1:
        #             sim.x(2)
        #             parity_meas.clear()
        #             parity_last = parity

        #     elif not parity:
        #         if sum([int(parity) for parity in parity_meas]) >= rounds - 1:
        #             sim.x(2)
        #             parity_meas.clear()
        #             parity_last = parity

        # elif(not mixed_model and lut_enable == False):
        #     if all([parity_meas[i] == 1 - parity for i in range(rounds)]):
        #         sim.x(2)
        #         parity_meas.clear()
        #         parity_last = parity

        # elif(mixed_model):
       
        if parity_last == 1 - parity:
            sim.x(2)



        #XX

        sim.h(data_0_idx)                                                        
        sim.h(data_1_idx)
        sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
        sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)
        

        # if(lut_enable and not mixed_model):
        #     if phase:
        #         if sum([int(phase) for phase in phase_meas]) <= 1:
        #             sim.x(2)
        #             phase_meas.clear()
        #             phase_last = phase

        #     elif not phase:
        #         if sum([int(phase) for phase in phase_meas]) >= rounds - 1:
        #             sim.x(2)
        #             phase_meas.clear()
        #             phase_last = phase

        # elif(not mixed_model and lut_enable == False):
        #     if all([phase_meas[i] == 1 - phase for i in range(rounds)]):
        #         sim.x(2)
        #         phase_meas.clear()
        #         phase_last = phase

        # elif(mixed_model):
        if phase_last == 1 - phase:
            sim.x(2)
        
        sim.h(data_0_idx)
        sim.h(data_1_idx)
        sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
        sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)


        if sim.measure_observable(stim.PauliString("Z_Z")) != parity or \
            sim.measure_observable(stim.PauliString("X_X")) != phase: 
                errors +=1
        phase_meas.clear()
        parity_meas.clear()
    
    #print(count)
        
    return errors




import numpy as np

def compare_4(runs = 1000,
                    parity = 1,
                    phase = 1,
                    cycles = 10,
                    rounds = 1,
                    after_clifford_depolarization_1 = 0.001,
                    after_clifford_depolarization_2 = 0.01,
                    after_reset_flip_probability = 0.001,
                    before_measure_flip_probability = 0.001,
                    before_round_data_depolarization = 0.001,
                    p_amp_ge = 0.01,
                    max_error = 0.02,
                    num_points = 10,
                    ancilla_fb = False,
                    break_experiment = False,
                    ancilla_reset = True
):
    p_amp_ge_array = np.linspace(0.00, max_error, num_points)
    # cycles_array = np.linspace(1, 20, 10)
    cycles_array = np.arange(1,10, 1)
    logical_err_array = []

    cycles_1 = []
    cycles_2 = []
    cycles_3 = []

    for cycles in cycles_array:  
        cycles_1.append(bell_universal_4(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization_1= after_clifford_depolarization_1, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability,\
            after_clifford_depolarization_2= after_clifford_depolarization_2,\
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = 0.0,\
                runs = runs, rounds=3, ancilla_fb = False,  ancilla_reset=True, ancilla_fb_adaptive=False)/runs)
        cycles_2.append(bell_universal_4(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization_1= after_clifford_depolarization_1, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
            after_clifford_depolarization_2= after_clifford_depolarization_2,\
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=3, ancilla_fb = False,ancilla_reset=True, 
                ancilla_fb_adaptive=False,  ancilla_multiple=False)/runs)
        cycles_3.append(bell_universal_4(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization_1= after_clifford_depolarization_1, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
            after_clifford_depolarization_2= after_clifford_depolarization_2,\
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=3, ancilla_fb = True,  ancilla_reset=True,
                ancilla_fb_adaptive=True, ancilla_multiple=True)/runs)

    
    
    
    import matplotlib.pyplot as plt
    
    plt.plot(cycles_array,cycles_1, label=f'raw: no ancilla fb, p_ge: {0}', color='r', marker='o')
    # param_1, vars_1 = scipy.optimize.curve_fit(lambda t,b: 1-np.exp(-b*t),  cycles_array,  cycles_1) 
    # plt.plot(cycles_array, 1-np.exp(-param_1[0]*cycles_array),label= f'fit: no fb, no damping (T1:{ 1/param_1[0]})',\
    #             linestyle = 'dotted', color='r', marker = 'x' )
    param_1, vars_1 = scipy.optimize.curve_fit(lambda t,b: b,  cycles_array,  cycles_1) 
    plt.plot(cycles_array, [param_1[0] for i in cycles_array],label= f'fit: no fb, p_ge: {0} (F:{ 1-param_1[0]})',\
                linestyle = 'dotted', color='r', marker = 'x' )
    
    plt.plot(cycles_array,cycles_2, label=f'raw: no ancilla fb, p_ge: {p_amp_ge}', color='b', marker='o')
    # param_2, vars_2 = scipy.optimize.curve_fit(lambda t,b: 1-np.exp(-b*t),  cycles_array,  cycles_2) 
    # plt.plot(cycles_array, 1-np.exp(-param_2[0]*cycles_array),label= f'fit: no fb, damping (T1:{ 1/param_2[0]})',\
    #             linestyle = 'dotted', color='b', marker = 'x' ) 
    param_2, vars_2 = scipy.optimize.curve_fit(lambda t,b: b,  cycles_array,  cycles_2) 
    plt.plot(cycles_array, [param_2[0] for i in cycles_array],label= f'fit: no ancilla fb, p_ge: {p_amp_ge} (F:{ 1-param_2[0]})',\
                linestyle = 'dotted', color='b', marker = 'x' )
    
    plt.plot(cycles_array,cycles_3, label=f'raw: ancilla fb, p_ge: {p_amp_ge}', color='g', marker='o')
    # param_3, vars_3 = scipy.optimize.curve_fit(lambda t,b: 1-np.exp(-b*t),  cycles_array,  cycles_3) 
    # plt.plot(cycles_array, 1-np.exp(-param_3[0]*cycles_array),label= f'fit: fb, damping (T1:{ 1/param_3[0]})',\
    #             linestyle = 'dotted', color='g', marker = 'x' )
    param_3, vars_3 = scipy.optimize.curve_fit(lambda t,b:b,  cycles_array,  cycles_3) 
    plt.plot(cycles_array,[param_3[0] for i in cycles_array],label= f'fit: ancilla fb,  p_ge: {p_amp_ge} (F:{ 1-param_3[0]})',\
                linestyle = 'dotted', color='g', marker = 'x' )
 

    plt.legend( bbox_to_anchor=(1.02, 0.1), loc='upper left', borderaxespad=0)
    # plt.title(f' Logical Bell-state error.vs. T1 ancilla decay during msmt. ($\Phi{phase, parity, }$ clifford_err: {after_clifford_depolarization}, data_err before rnd: {before_round_data_depolarization}, runs: {runs})')
    plt.title(f"phase:{phase}, parity: {parity}") 
    plt.xlabel('cycles')
    plt.ylabel('p_error')

    



#This new version implements changes discussed with Sebastian Krinner on 20.11.23
#Different error probabilities for 1 and 2 QB gates
#No more Ancilla reset -> need to track its state manually
#For rounds = 3, have option to decode errors in a LUT

def bell_universal_3(p_eg = 0.0,
                     p_ge = 0.01, 
                     cycles = 10,
                     phase = 0, 
                     parity = 0, 
                     ancilla_fb = False, 
                     ancilla_fb_adaptive = False,
                     rounds = 1,
                     runs = 1000,
                     after_reset_flip_probability=0.001, 
                     after_clifford_depolarization_1= 0.001, 
                     after_clifford_depolarization_2= 0.01, 
                     before_measure_flip_probability= 0.001, 
                     before_round_data_depolarization=0.001,
                     break_experiment = False,
                     ancilla_multiple = False,
                     mixed_model = False, 
                     ancilla_reset = True,
                     lut_enable = False):

    """

    arguments:

    cycles: How often measure ZZ and XX stabilizers
    p_ge: Decoherene probability from 1 to 0 state
    p_eg: Decoherene probability from 0 to 1 state
    parity: In which parity to stabilize the state
    phase: In which phase to stabilize the state
    ancilla_fb: If the true apply X gate before ancilla measurment if expected value is 1
    state_switch: Option to stabilize phi00 and switch to phi11 in last cycle
    rounds: Based on how many rounds one does feedback
    runs: How many experiments
    break_experiment: 
    ancilla_reset: Indicate whether we reset the ancilla to 0 after each msmt
    lut_enable: If rounds >= 3, decode msmts via a LUT

    
    """
    errors = 0
    count = 0
    ancilla_idx = 1
    data_0_idx = 0
    data_1_idx = 2
    
    for l in range(runs):
        sim = stim.TableauSimulator()

         #FIFO queue of measurments
        parity_meas = collections.deque(maxlen = rounds)
        phase_meas = collections.deque(maxlen = rounds)              

        flip = False
        parity_last = parity
        phase_last = phase
        break_loop = False
        last_ms = False
        last_ms_helper = False #Use this variable just as a helper to store last_ms value

        #Initialisation circuit
        sim.reset(data_0_idx)
        sim.reset(ancilla_idx)
        sim.reset(data_1_idx)
        sim.x_error(data_0_idx, p = after_reset_flip_probability)                
        sim.x_error(ancilla_idx, p = after_reset_flip_probability) 
        sim.x_error(data_1_idx, p = after_reset_flip_probability)

        sim.h(data_0_idx)
        sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
        sim.h(data_1_idx)    
        sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)       

        #If we want to perform ancilla fb we need one cycle to reach a fidelity
        #at which it makes sense to predict the ancillas state

        if(ancilla_fb):
            for i in range(int(rounds >= 2 and ancilla_fb_adaptive)+1):
                
                #ZZ, no ancilla feedback
                sim.cnot(data_0_idx,ancilla_idx)                                               
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)
                            
                anc = sim.peek_bloch(ancilla_idx)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                #Symmetric Readout error
                sim.x_error(ancilla_idx, p = before_measure_flip_probability)    
                parity_last = sim.measure(ancilla_idx)

                if ancilla_reset:
                    sim.reset(ancilla_idx)
                    sim.x_error(ancilla_idx, p = after_reset_flip_probability)
                
                elif not ancilla_reset:
                    last_ms_helper = last_ms
                    last_ms = parity_last
                    parity_last = parity_last ^ last_ms_helper

                if parity_last == 1 - parity:
                    sim.x(data_1_idx)
                    parity_last = parity
            



                #XX, no ancilla feedback 
                sim.h(data_0_idx)                                                        
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)
                
                sim.cnot(data_0_idx,ancilla_idx)
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)

                anc = sim.peek_bloch(1)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                sim.x_error(ancilla_idx, p = before_measure_flip_probability)
                phase_last = sim.measure(ancilla_idx)
                
                if ancilla_reset:
                    sim.reset(ancilla_idx)
                    sim.x_error(ancilla_idx, p = after_reset_flip_probability)
                
                elif not ancilla_reset:
                    last_ms_helper = last_ms
                    last_ms = phase_last
                    phase_last = phase_last ^ last_ms_helper

                if phase_last == 1 - phase:
                    sim.x(data_1_idx)
                    phase_last = phase

                sim.h(data_0_idx)
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)


        for j in range(cycles):
            if(break_loop == False):
                #ZZ 
                sim.cnot(data_0_idx,ancilla_idx)                                               
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)

                #For ancilla fb if the last stabilizer msmt was 1 we still expect it to 1 on average 
                #so we flipped to increase RO fidelity and set flip to 1 X flip gate error
                #If correct parity is 1 we still expect it to 1 on average so we flipped to
                #increase RO fidelity and set flip to 1 X flip gate error
                #The second part enables making ancilla fb based on the last #rounds - 1 cycles
                if ancilla_fb:                                         
                    if(ancilla_fb_adaptive and ancilla_multiple == False):                                
                        if parity_last:                                     
                            sim.x(ancilla_idx)                                        
                            flip = True

                    elif(ancilla_fb_adaptive and ancilla_multiple == True):
                        if(len(parity_meas) == rounds):
                             if [parity_meas[0], parity_meas[1], parity_meas[2]] in \
                                [[1-parity, 1-parity, 1-parity], [parity, 1-parity, 1-parity], [1-parity, parity, 1-parity], [1-parity, 1-parity, parity]]:
                                sim.x(ancilla_idx)                                        
                                flip = True

                        elif(len(parity_meas) < rounds):
                            if parity:
                                sim.x(ancilla_idx)                                        
                                flip = True
            
                    elif ancilla_fb_adaptive == False:                      
                        if parity:                                           
                            sim.x(ancilla_idx)                                        
                            flip = True
                            
                anc = sim.peek_bloch(ancilla_idx)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                #Symmetric Readout error
                sim.x_error(ancilla_idx, p = before_measure_flip_probability)    
                parity_last = sim.measure(ancilla_idx)
                
                if ancilla_reset:
                    sim.reset(ancilla_idx)
                    sim.x_error(ancilla_idx, p = after_reset_flip_probability)
                
                elif not ancilla_reset:
                    last_ms_helper = last_ms
                    last_ms = parity_last
                    parity_last = parity_last ^ last_ms_helper

                #If we performed ancilla fb we need to invert our msmt result
                if ancilla_fb:                                              
                    if flip:                                             
                        parity_last = 1 - parity_last
                        flip = False
                
                parity_meas.append(parity_last)
                
                if(rounds == 3 and lut_enable and len(parity_meas) == rounds and not mixed_model):
                    if [parity_meas[0], parity_meas[1], parity_meas[2]] in \
                        [[1-parity, 1-parity, 1-parity], [parity, 1-parity, 1-parity], [1-parity, parity, 1-parity], [1-parity, 1-parity, parity]]:
                        sim.x(2)
                        parity_meas.clear()
                        parity_last = parity

                if(len(parity_meas) == rounds and not mixed_model and lut_enable == False):
                    if all([parity_meas[i] == 1 - parity for i in range(rounds)]):
                        sim.x(2)
                        parity_meas.clear()
                        parity_last = parity

                if(mixed_model):
                    if parity_last == 1 - parity:
                        sim.x(2)




                #XX 
                sim.h(data_0_idx)                                                        
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)
                
                sim.cnot(data_0_idx,ancilla_idx)
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization_2)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization_2)

                if ancilla_fb:                                         
                    if(ancilla_fb_adaptive and ancilla_multiple == False):                               
                        if phase_last:                                     
                            sim.x(ancilla_idx)                                        
                            flip = True
                    
                    elif(ancilla_fb_adaptive and ancilla_multiple == True):
                        if(len(phase_meas) == rounds):
                            if [phase_meas[0], phase_meas[1], phase_meas[2]] in \
                                [[1-phase, 1-phase, 1-phase], [phase, 1-phase, 1-phase], [1-phase, phase, 1-phase], [1-phase, 1-phase, phase]]:
                                    sim.x(ancilla_idx)                                        
                                    flip = True
                        
                        elif(len(phase_meas) < rounds):
                            if phase:
                                sim.x(ancilla_idx)                                        
                                flip = True

                    elif ancilla_fb_adaptive == False:                      
                        if phase:                                          
                            sim.x(ancilla_idx)                                        
                            flip = True
                
                anc = sim.peek_bloch(1)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                sim.x_error(ancilla_idx, p = before_measure_flip_probability)
                phase_last = sim.measure(1)
                
                if ancilla_reset:
                    sim.reset(ancilla_idx)
                    sim.x_error(ancilla_idx, p = after_reset_flip_probability)
                
                elif not ancilla_reset:
                    last_ms_helper = last_ms
                    last_ms = phase_last
                    phase_last = phase_last ^ last_ms_helper
                    
                if ancilla_fb:
                    if flip:
                        phase_last = 1 - phase_last
                        flip = False

                phase_meas.append(phase_last)

                if(rounds == 3 and lut_enable and len(phase_meas) == rounds and not mixed_model):
                    if [phase_meas[0], phase_meas[1], phase_meas[2]] in \
                        [[1-phase, 1-phase, 1-phase], [phase, 1-phase, 1-phase], [1-phase, phase, 1-phase], [1-phase, 1-phase, phase]]:
                        sim.x(2)
                        phase_meas.clear()
                        phase_last = phase

                if(len(phase_meas) == rounds and not mixed_model and lut_enable == False):
                    if all([phase_meas[i] == 1 - phase for i in range(rounds)]):
                        sim.x(2)
                        phase_meas.clear()
                        phase_last = phase

                if(mixed_model):
                    if phase_last == 1 - phase:
                        sim.x(2)
        
                sim.h(data_0_idx)
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization_1)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization_1)


                break_loop = len(parity_meas) == rounds and len(phase_meas) == rounds and \
                                all([parity_meas[i] == parity for i in range(len(parity_meas))]) \
                                and all([phase_meas[i]== phase for i in range(len(phase_meas))]) \
                                and break_experiment

        
        if sim.measure_observable(stim.PauliString("Z_Z")) != parity or \
            sim.measure_observable(stim.PauliString("X_X")) != phase: 
                errors +=1
        phase_meas.clear()
        parity_meas.clear()
    
    #print(count)
        
    return errors





#NOTE: This is the new verion of the function below where I added the features we discussed on Friday. 
#Some functionalites are still unverified

def bell_universal_new(p_eg = 0.0, 
                     p_ge = 0.01, 
                     cycles = 10,
                     phase = 0, 
                     parity = 0, 
                     ancilla_fb = False, 
                     ancilla_fb_adaptive = False,
                     rounds = 1,
                     runs = 1000,
                     after_reset_flip_probability=0.001, 
                     after_clifford_depolarization= 0.001, 
                     before_measure_flip_probability= 0.001, 
                     before_round_data_depolarization=0.001,
                     break_experiment = False,
                     ancilla_multiple = False,
                     mixed_model = False):

    """

    arguments:

    cycles: How often measure ZZ and XX stabilizers
    p_ge: Decoherene probability from 1 to 0 state
    p_eg: Decoherene probability from 0 to 1 state
    parity: In which parity to stabilize the state
    phase: In which phase to stabilize the state
    ancilla_fb: If the true apply X gate before ancilla measurment if expected value is 1
    state_switch: Option to stabilize phi00 and switch to phi11 in last cycle
    rounds: Based on how many rounds one does feedback
    runs: How many experiments
    break_experiment: If we expect a high state fidelity should the experiment end earlier
    ancilla_multiple: Use multiple msmt rounds to perform ancilla fb
    mixed_model: Do fb based on one cycle, but end msmt only if #rounds cycle indicate this

    
    """
    errors = 0
    count = 0
    ancilla_idx = 1
    data_0_idx = 0
    data_1_idx = 2
    
    for l in range(runs):
        sim = stim.TableauSimulator()

         #FIFO queue of measurments
        parity_meas = collections.deque(maxlen = rounds)
        phase_meas = collections.deque(maxlen = rounds)              

        flip = False
        parity_last = parity
        phase_last = phase


        #Initialisation circuit
        sim.reset(data_0_idx)
        sim.reset(ancilla_idx)
        sim.reset(data_1_idx)
        sim.x_error(data_0_idx, p = after_reset_flip_probability)                
        sim.x_error(ancilla_idx, p = after_reset_flip_probability) 
        sim.x_error(data_1_idx, p = after_reset_flip_probability)

        sim.h(data_0_idx)
        sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
        sim.h(data_1_idx)    
        sim.depolarize1(data_1_idx, p = after_clifford_depolarization)       

        flip = False
        parity_last = parity
        phase_last = phase

        break_loop = False
        #If we want to perform ancilla fb we need one cycle to reach a fidelity
        #at which it makes sense to predict the ancillas state

        if(ancilla_fb):
            for i in range(int(rounds >= 2 and ancilla_fb_adaptive)+1):
                
                #ZZ, no ancilla feedback
                sim.cnot(data_0_idx,ancilla_idx)                                               
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization)
                            
                anc = sim.peek_bloch(ancilla_idx)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                #Symmetric Readout error
                sim.x_error(ancilla_idx, p = before_measure_flip_probability)    
                parity_last = sim.measure(ancilla_idx)
                sim.reset(ancilla_idx)
                sim.x_error(ancilla_idx, p = after_reset_flip_probability)

                if parity_last == 1 - parity:
                    sim.x(data_1_idx)
                    parity_last = parity
        


                #XX, no ancilla feedback 
                sim.h(data_0_idx)                                                        
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization)
                
                sim.cnot(data_0_idx,ancilla_idx)
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization)

                anc = sim.peek_bloch(1)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                sim.x_error(ancilla_idx, p = before_measure_flip_probability)
                phase_last = sim.measure(1)
                sim.reset(ancilla_idx)
                sim.x_error(ancilla_idx, p = after_reset_flip_probability)

                if phase_last == 1 - phase:
                    sim.x(data_1_idx)
                    phase_last = phase

                sim.h(data_0_idx)
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization)


        for j in range(cycles):
            if(break_loop == False):
                #ZZ 
                sim.cnot(data_0_idx,ancilla_idx)                                               
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization)

                #For ancilla fb if the last stabilizer msmt was 1 we still expect it to 1 on average 
                #so we flipped to increase RO fidelity and set flip to 1 X flip gate error
                #If correct parity is 1 we still expect it to 1 on average so we flipped to
                #increase RO fidelity and set flip to 1 X flip gate error
                #The second part enables making ancilla fb based on the last #rounds - 1 cycles
                if ancilla_fb:                                         
                    if(ancilla_fb_adaptive and ancilla_multiple == False):                                
                        if parity_last:                                     
                            sim.x(ancilla_idx)                                        
                            flip = True

                    elif(ancilla_fb_adaptive and ancilla_multiple == True):
                         if(len(parity_meas) == rounds-1):
                            for i in range(rounds-1):
                                if parity_meas[i] == 1:
                                    if(i == rounds - 2):
                                        sim.x(ancilla_idx)                                        
                                        flip = True
                                elif parity_meas[i] == 0:
                                    break
            
                    elif ancilla_fb_adaptive == False:                      
                        if parity:                                           
                            sim.x(ancilla_idx)                                        
                            flip = True
                            
                anc = sim.peek_bloch(ancilla_idx)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                #Symmetric Readout error
                sim.x_error(ancilla_idx, p = before_measure_flip_probability)    
                parity_last = sim.measure(ancilla_idx)
                sim.reset(ancilla_idx)
                sim.x_error(ancilla_idx, p = after_reset_flip_probability)

                #If we performed ancilla fb we need to invert our msmt result
                if ancilla_fb:                                              
                    if flip:                                             
                        parity_last = 1 - parity_last
                        flip = False
                
                parity_meas.append(parity_last)
                
                if(len(parity_meas) == rounds and not mixed_model):
                    for i in range(rounds):
                        if parity_meas[i] == parity:
                            break
                        elif parity_meas[i] == 1 - parity:
                            if(i == rounds - 1):
                                sim.x(2)
                                parity_meas.clear()
                                parity_last = parity

                if(mixed_model):
                    if parity_last == 1 - parity:
                        sim.x(2)



                #XX 
                sim.h(data_0_idx)                                                        
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization)
                
                sim.cnot(data_0_idx,ancilla_idx)
                sim.cnot(data_1_idx,ancilla_idx)
                sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization)
                sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization)

                if ancilla_fb:                                         
                    if(ancilla_fb_adaptive and ancilla_multiple == False):                               
                        if phase_last:                                     
                            sim.x(ancilla_idx)                                        
                            flip = True
                    
                    elif(ancilla_fb_adaptive and ancilla_multiple == True):
                         if(len(phase_meas) == rounds-1):
                            for i in range(rounds-1):
                                if phase_meas[i] == 1:
                                    if(i == rounds - 2):
                                        sim.x(ancilla_idx)                                        
                                        flip = True
                                elif phase_meas[i] == 0:
                                    break
                    
                    elif ancilla_fb_adaptive == False:                      
                        if phase:                                          
                            sim.x(ancilla_idx)                                        
                            flip = True
                
                anc = sim.peek_bloch(1)
                if anc == stim.PauliString("-Z"):
                    sim.x_error(ancilla_idx, p = p_ge)
                else:
                    sim.x_error(ancilla_idx, p = p_eg)

                sim.x_error(ancilla_idx, p = before_measure_flip_probability)
                phase_last = sim.measure(1)
                sim.reset(ancilla_idx)
                sim.x_error(ancilla_idx, p = after_reset_flip_probability)
                    
                if ancilla_fb:
                    if flip:
                        phase_last = 1 - phase_last
                        flip = False

                phase_meas.append(phase_last)
                if(len(phase_meas) == rounds and not mixed_model):
                    for i in range(rounds):
                        if phase_meas[i] == phase:
                            break
                        elif phase_meas[i] == 1 - phase:
                            if(i == rounds - 1):
                                sim.x(2)                          
                                phase_meas.clear()
                                phase_last = phase

                if(mixed_model):
                    if phase_last == 1 - phase:
                        sim.x(2)
        
                sim.h(data_0_idx)
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
                sim.depolarize1(data_1_idx, p = after_clifford_depolarization)


                break_loop = len(parity_meas) == rounds and len(phase_meas) == rounds and \
                                all([parity_meas[i] == parity for i in range(len(parity_meas))]) \
                                and all([phase_meas[i]== phase for i in range(len(phase_meas))]) \
                                and break_experiment

        
        if sim.measure_observable(stim.PauliString("Z_Z")) != parity or \
            sim.measure_observable(stim.PauliString("X_X")) != phase: 
                errors +=1
        phase_meas.clear()
        parity_meas.clear()
    
    #print(count)
        
    return errors





import numpy as np

def compare_adapt(runs = 1000,
                    parity = 1,
                    phase = 1,
                    cycles = 10,
                    rounds = 1,
                    after_clifford_depolarization = 0.001,
                    after_reset_flip_probability = 0.001,
                    before_measure_flip_probability = 0.001,
                    before_round_data_depolarization = 0.001,
                    max_error = 0.02,
                    num_points = 10,
                    ancilla_fb = False,
                    break_experiment = False,
):
    p_amp_ge_array = np.linspace(0.00, max_error, num_points)
    logical_err_array = []

    cycles_1 = []
    cycles_2 = []
    cycles_3 = []
    cycles_4 = []
    cycles_5 = []
    cycles_6 = []

    for p_amp_ge in p_amp_ge_array:  
        cycles_1.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge,\
                runs = runs, rounds=1, ancilla_fb = False, break_experiment=break_experiment)/runs)
        cycles_2.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=1, ancilla_fb = True, break_experiment=break_experiment)/runs)
        cycles_3.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=2, ancilla_fb = False, break_experiment=break_experiment)/runs)
        cycles_4.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=2, ancilla_fb = True, mixed_model=False, break_experiment=break_experiment)/runs)
        cycles_5.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=3, ancilla_fb = True, ancilla_multiple=False, ancilla_fb_adaptive=False, break_experiment=break_experiment)/runs)


    import matplotlib.pyplot as plt
    plt.plot(p_amp_ge_array,cycles_1, label='1, rounds = 1', color='r', marker='o')
    plt.plot(p_amp_ge_array,cycles_2, label='2, rounds = 1, afb', color='b', marker='o')
    plt.plot(p_amp_ge_array,cycles_3, label='3, rounds = 2', color='g', marker='o')
    plt.plot(p_amp_ge_array,cycles_4, label='3.1, rounds = 2, afb', color='r', marker='+')
    plt.plot(p_amp_ge_array,cycles_5, label='4, rounds = 3', color='b', marker='+')
    
    plt.legend()
    # plt.title(f' Logical Bell-state error.vs. T1 ancilla decay during msmt. ($\Phi{phase, parity, }$ clifford_err: {after_clifford_depolarization}, data_err before rnd: {before_round_data_depolarization}, runs: {runs})')
    plt.title(f"phase:{phase}, parity: {parity}, break experiment: {break_experiment}") 
    plt.xlabel('p_X conditioned on ancilla in |1>')
    plt.ylabel('p_error, Logical Z')




import numpy as np

def compare_best(runs = 1000,
                    parity = 1,
                    phase = 1,
                    cycles = 10,
                    rounds = 1,
                    after_clifford_depolarization = 0.01,
                    after_reset_flip_probability = 0.01,
                    before_measure_flip_probability = 0.01,
                    before_round_data_depolarization = 0.01,
                    max_error = 0.02,
                    num_points = 10,
                    ancilla_fb = False,
                    break_experiment = False,
):
    p_amp_ge_array = np.linspace(0.00, max_error, num_points)
    logical_err_array = []

    cycles_1 = []
    cycles_2 = []
    cycles_3 = []
    cycles_4 = []
    cycles_5 = []
    cycles_6 = []

    for p_amp_ge in p_amp_ge_array:  
        cycles_1.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge,\
                runs = runs, rounds=2, ancilla_fb = True, break_experiment=True)/runs)
        cycles_2.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=3, ancilla_fb = True, break_experiment=True)/runs)
        cycles_3.append(bell_universal_new(parity = parity, phase = phase, cycles = cycles, \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = p_amp_ge, \
                runs = runs, rounds=3, ancilla_fb = True, mixed_model = True,break_experiment=True)/runs)



    import matplotlib.pyplot as plt
    plt.plot(p_amp_ge_array,cycles_1, label='rounds = 2, afb', color='r', marker='o')
    plt.plot(p_amp_ge_array,cycles_2, label='rounds = 3, afb', color='b', marker='o')
    plt.plot(p_amp_ge_array,cycles_3, label='rounds = 3, afb, mixed', color='g', marker='o')

    
    plt.legend()
    # plt.title(f' Logical Bell-state error.vs. T1 ancilla decay during msmt. ($\Phi{phase, parity, }$ clifford_err: {after_clifford_depolarization}, data_err before rnd: {before_round_data_depolarization}, runs: {runs})')
    plt.title(f"phase:{phase}, parity: {parity}, break experiment: {break_experiment}, ancilla_fb: {ancilla_fb}") 
    plt.xlabel('p_X conditioned on ancilla in |1>')
    plt.ylabel('p_error, Logical Z')
    
    
    
    
    
    
    
    
def errors_sym_vs_asym(p_eg = 0.0,  
                     cycles = 10,
                     phase = 0, 
                     parity = 0, 
                     ancilla_fb_adaptive = False,
                     runs = 300, 
                     after_clifford_depolarization= 0.001, 
                     after_reset_flip_probability = 0.0,
                     before_measure_flip_probability= 0.01, 
                     before_round_data_depolarization=0.00,
                     state_switch = False,
                     max_ge = 0.02,
                     max_flip_prob = 0.02,
                     num_points = 10,
                     rounds = 1,
                     ancilla_fb = False,
                     break_experiment = False
                     ):
   
    p_ge_range = np.linspace(0.0, max_ge, num_points)
    p_x_err_range = np.linspace(0.0, max_flip_prob, num_points)

    errors_1 = np.zeros((num_points, num_points))
    errors_2 = np.zeros((num_points, num_points))
    errors_3 = np.zeros((num_points, num_points))
    errors_4 = np.zeros((num_points, num_points))
    errors_5 = np.zeros((num_points, num_points))

    for j in range(num_points):
        for i in range(num_points):
            errors_1[i][j] = (bell_universal_new(p_ge = p_ge_range[i], 
                                               before_measure_flip_probability=p_x_err_range[j],
                                               after_clifford_depolarization= after_clifford_depolarization, 
                                                after_reset_flip_probability = after_reset_flip_probability,
                                                before_round_data_depolarization=before_round_data_depolarization,
                                                cycles = cycles, rounds=1, parity=parity, phase=phase, runs = runs, 
                                                ancilla_fb = False, break_experiment= break_experiment)/runs)

            errors_2[i][j] = (bell_universal_new(p_ge = p_ge_range[i], 
                                               before_measure_flip_probability=p_x_err_range[j],
                                               after_clifford_depolarization= after_clifford_depolarization, 
                                                after_reset_flip_probability = after_reset_flip_probability,
                                                before_round_data_depolarization=before_round_data_depolarization,
                                                cycles = cycles, rounds=1, parity=parity, phase=phase, runs = runs,
                                                ancilla_fb=True, break_experiment= break_experiment)/runs)

            errors_3[i][j] = (bell_universal_new(p_ge = p_ge_range[i], 
                                               before_measure_flip_probability=p_x_err_range[j],
                                               after_clifford_depolarization= after_clifford_depolarization, 
                                                after_reset_flip_probability = after_reset_flip_probability,
                                                before_round_data_depolarization=before_round_data_depolarization,
                                                cycles = cycles, rounds=3, parity=parity, phase=phase, runs = runs,
                                                ancilla_fb=False, break_experiment= break_experiment)/runs)

            errors_4[i][j] = (bell_universal_new(p_ge = p_ge_range[i], 
                                               before_measure_flip_probability=p_x_err_range[j],
                                               after_clifford_depolarization= after_clifford_depolarization, 
                                                after_reset_flip_probability = after_reset_flip_probability,
                                                before_round_data_depolarization=before_round_data_depolarization,
                                                cycles = cycles, rounds=3, parity=parity, phase=phase, runs = runs,
                                                ancilla_fb=True, ancilla_fb_adaptive=True, break_experiment= break_experiment)/runs)

            errors_5[i][j] = (bell_universal_new(p_ge = p_ge_range[i],
                                               before_measure_flip_probability=p_x_err_range[j],
                                               after_clifford_depolarization= after_clifford_depolarization, 
                                                after_s17reset_flip_probability = after_reset_flip_probability,
                                                before_round_data_depolarization=before_round_data_depolarization,
                                                cycles = cycles, rounds=3, parity=parity, phase=phase, runs = runs,
                                                ancilla_fb=True, ancilla_fb_adaptive=True, ancilla_multiple=True, break_experiment= break_experiment)/runs)


    return errors_1, errors_2, errors_3, errors_4, errors_5



import numpy as np

def compare_cycles(runs = 1000,
                    parity = 1,
                    phase = 1,
                    rounds = 1,
                    after_clifford_depolarization = 0.001,
                    after_reset_flip_probability = 0.001,
                    before_measure_flip_probability = 0.001,
                    before_round_data_depolarization = 0.001,
                    max_error = 0.02,
                    num_points = 10,
                    ancilla_fb = False,
                    break_experiment = False
):
    cycles_array = np.linspace(2, 20, 10)

    logical_err_array = []

    cycles_1 = []
    cycles_2 = []
    cycles_3 = []
    cycles_4 = []
    cycles_5 = []
    

    for cycles in cycles_array:  
        cycles_1.append(bell_universal_new(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = 0.01,\
                runs = runs, rounds=1, ancilla_fb = False, break_experiment=break_experiment)/runs)
        cycles_2.append(bell_universal_new(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = 0.01, \
                runs = runs, rounds=1, ancilla_fb = True, break_experiment=break_experiment)/runs)
        cycles_3.append(bell_universal_new(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = 0.01, \
                runs = runs, rounds=2, ancilla_fb = False, break_experiment=break_experiment)/runs)
        cycles_4.append(bell_universal_new(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = 0.01, \
                runs = runs, rounds=2, ancilla_fb = True, mixed_model=False, break_experiment=break_experiment)/runs)
        cycles_5.append(bell_universal_new(parity = parity, phase = phase, cycles = int(cycles), \
            after_clifford_depolarization= after_clifford_depolarization, after_reset_flip_probability= after_reset_flip_probability,  \
            before_measure_flip_probability = before_measure_flip_probability, \
                before_round_data_depolarization=before_round_data_depolarization,  p_ge = 0.01, \
                runs = runs, rounds=3, ancilla_fb = True, ancilla_multiple=False, ancilla_fb_adaptive=False, break_experiment=break_experiment)/runs)


    import matplotlib.pyplot as plt
    plt.plot(cycles_array,cycles_1, label='1, rounds = 1', color='r', marker='o')
    plt.plot(cycles_array,cycles_2, label='2, rounds = 1, afb', color='b', marker='o')
    plt.plot(cycles_array,cycles_3, label='3, rounds = 2', color='g', marker='o')
    plt.plot(cycles_array,cycles_4, label='3.1, rounds = 2, afb', color='r', marker='+')
    plt.plot(cycles_array,cycles_5, label='4, rounds = 3', color='b', marker='+')
    
    plt.legend()
    # plt.title(f' Logical Bell-state error.vs. T1 ancilla decay during msmt. ($\Phi{phase, parity, }$ clifford_err: {after_clifford_depolarization}, data_err before rnd: {before_round_data_depolarization}, runs: {runs})')
    plt.title(f"phase:{phase}, parity: {parity}, break experiment: {break_experiment}") 
    plt.xlabel('cycles')
    plt.ylabel('p_error, Logical Z')





#Feedback based on last cycle
def bell_universal(p_eg = 0.0, 
                     p_ge = 0.01, 
                     cycles = 10,
                     phase = 0, 
                     parity = 0, 
                     ancilla_fb = False, 
                     ancilla_fb_adaptive = False,
                     rounds = 1,
                     runs = 1000,
                     after_reset_flip_probability=0.001, 
                     after_clifford_depolarization= 0.001, 
                     before_measure_flip_probability= 0.001, 
                     before_round_data_depolarization=0.001,
                     state_switch = False,
                     fidelity_parity = 0.8,
                     fidelity_phase = 0.8):

    """

    arguments:

    cycles: How often measure ZZ and XX stabilizers
    p_ge: Decoherene probability from 1 to 0 state
    p_eg: Decoherene probability from 0 to 1 state
    parity: In which parity to stabilize the state
    phase: In which phase to stabilize the state
    ancilla_fb: If the true apply X gate before ancilla measurment if expected value is 1
    state_switch: Option to stabilize phi00 and switch to phi11 in last cycle
    rounds: Based on how many rounds one does feedback
    runs: How many experiments

    
    """
    errors = 0

    ancilla_idx = 1
    data_0_idx = 0
    data_1_idx = 2
        

    for l in range(runs):
        sim = stim.TableauSimulator()

        #Initialisation circuit
        sim.reset(data_0_idx)
        sim.reset(ancilla_idx)
        sim.reset(data_1_idx)
        #sim.x_error(data_0_idx, p = after_reset_flip_probability)                
        #sim.x_error(ancilla_idx, p = after_reset_flip_probability) 
        #sim.x_error(data_1_idx, p = after_reset_flip_probability)

        if(phase):
            sim.x(data_0_idx)
            #sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
        
        if(parity):
            sim.x(data_1_idx)
            #sim.depolarize1(data_1_idx, p = after_clifford_depolarization)

        sim.h(data_0_idx)
        #sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
        sim.cnot(data_0_idx,data_1_idx)
        #sim.depolarize2(data_0_idx,data_1_idx, p = after_clifford_depolarization)

        sim.x_error(data_1_idx, p = 1.0-fidelity_parity)

        sim.h(data_0_idx)
        sim.h(data_1_idx)
        sim.x_error(data_1_idx, p = 1.0-fidelity_phase)
        sim.h(data_0_idx)
        sim.h(data_1_idx)

        #FIFO queue of measurments
        parity_meas = collections.deque(maxlen = rounds)
        phase_meas = collections.deque(maxlen = rounds)              

        flip = False
        parity_last = parity
        phase_last = phase

        #sim.depolarize1(data_0_idx, data_1_idx, p = before_round_data_depolarization)
        #sim.depolarize1(data_1_idx, data_1_idx, p = before_round_data_depolarization)

        for j in range(cycles):
            
            #ZZ 
            sim.cnot(data_0_idx,ancilla_idx)                                               
            sim.cnot(data_1_idx,ancilla_idx)
            sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization)
            sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization)

            #For ancilla fb if the last stabilizer msmt was 1 we still expect it to 1 on average 
            #so we flipped to increase RO fidelity and set flip to 1 X flip gate error
            #If correct parity is 1 we still expect it to 1 on average so we flipped to
            #increase RO fidelity and set flip to 1 X flip gate error
            if ancilla_fb:                                         
                if(ancilla_fb_adaptive):                                
                    if parity_last:                                     
                        sim.x(ancilla_idx)                                        
                        flip = True
                
                elif ancilla_fb_adaptive == False:                      
                    if parity:                                           
                        sim.x(ancilla_idx)                                        
                        flip = True
                        
            anc = sim.peek_bloch(ancilla_idx)
            if anc == stim.PauliString("-Z"):
                sim.x_error(ancilla_idx, p = p_ge)
            else:
                sim.x_error(ancilla_idx, p = p_eg)

            #Symmetric Readout error
            sim.x_error(ancilla_idx, p = before_measure_flip_probability)    
            parity_last = sim.measure(ancilla_idx)
            sim.reset(ancilla_idx)
            sim.x_error(ancilla_idx, p = after_reset_flip_probability)

            #If we performed ancilla fb we need to invert our msmt result
            if ancilla_fb:                                              
                if flip:                                             
                    parity_last = 1 - parity_last
                    flip = False
            
            parity_meas.append(parity_last)
                
            if(len(parity_meas) == rounds):
                for i in range(rounds):
                    if parity_meas[i] == parity:
                        break
                    elif parity_meas[i] == 1 - parity:
                        if(i == rounds - 1):
                            sim.x(2)
                            parity_meas.clear()
                            parity_last = parity

            

            #XX 
            sim.h(data_0_idx)                                                        
            sim.h(data_1_idx)
            sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
            sim.depolarize1(data_1_idx, p = after_clifford_depolarization)
            
            sim.cnot(data_0_idx,ancilla_idx)
            sim.cnot(data_1_idx,ancilla_idx)
            sim.depolarize2(data_0_idx, ancilla_idx, p = after_clifford_depolarization)
            sim.depolarize2(data_1_idx, ancilla_idx, p = after_clifford_depolarization)

            if ancilla_fb:                                         
                if(ancilla_fb_adaptive):                               
                    if phase_last:                                     
                        sim.x(ancilla_idx)                                        
                        flip = True
                
                elif ancilla_fb_adaptive == False:                      
                    if phase:                                          
                        sim.x(ancilla_idx)                                        
                        flip = True
            
            anc = sim.peek_bloch(1)
            if anc == stim.PauliString("-Z"):
                sim.x_error(ancilla_idx, p = p_ge)
            else:
                sim.x_error(ancilla_idx, p = p_eg)

            sim.x_error(ancilla_idx, p = before_measure_flip_probability)
            phase_last = sim.measure(1)
            sim.reset(ancilla_idx)
            sim.x_error(ancilla_idx, p = after_reset_flip_probability)
                
            if ancilla_fb:
                if flip:
                    phase_last = 1 - phase_last
                    flip = False

            phase_meas.append(phase_last)
            if(len(phase_meas) == rounds):
                for i in range(rounds):
                    if phase_meas[i] == phase:
                        break
                    elif phase_meas[i] == 1 - phase:
                        if(i == rounds - 1):
                            sim.x(2)                          
                            phase_meas.clear()
                            phase_last = phase
        
            sim.h(data_0_idx)
            sim.h(data_1_idx)
            sim.depolarize1(data_0_idx, p = after_clifford_depolarization)
            sim.depolarize1(data_1_idx, p = after_clifford_depolarization)

        
        if sim.measure_observable(stim.PauliString("Z_Z")) != parity or \
            sim.measure_observable(stim.PauliString("X_X")) != phase: 
                errors +=1
        
    return errors