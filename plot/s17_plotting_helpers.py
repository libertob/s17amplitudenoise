import stim
import numpy as np
import random
import matplotlib.pyplot as plt
import scipy
import pymatching
import json



## S17 helper functions for simulating a S17 circuit without reset. 

def qb_num(qb_label:str, start_from_0:bool = True):
    """
    Takes a human readable qubit label in the form D1...D9, X1...X4, or Z1...Z4
    and outputs the corresponding qubit number between 1 and 17 according to a
    mapping defined here.
    """
    mapping = {
        "D1": 2,
        "D2": 5,
        "D3": 11,
        "D4": 3,
        "D5": 9,
        "D6": 15,
        "D7": 7,
        "D8": 13,
        "D9": 16,
        "X1": 6,
        "X2": 4,
        "X3": 14,
        "X4": 12,
        "Z1": 1,
        "Z2": 8,
        "Z3": 10,
        "Z4": 17,
    }

    return mapping[qb_label] - start_from_0

def transf_01_to_pm1(val) -> int:
    """Convert measurement results in the format 0/1 to the format +1/-1.
    Both strings and ints are accepted and also '_' as another label for 0.
    """
    match val:
        case 0:
            return 1
        case False:
            return 1
        case "0":
            return 1
        case "_":
            return 1
        case 1:
            return -1
        case True:
            return -1
        case "1":
            return -1
        case _:
            raise ValueError(f"Invalid input of {val}!")

def get_product_from_str(input) -> int:
    """Transforms a string of measurements into the product of the measurement results.
    Input can be either an int, str or bool and if it is a string it can have length 1 or longer (i.e. measurements of a single qubit are also possible).
    The input is expected to be in the 0/1 format and is converted to the +1/-1 format.
    """
    res = 1
    if hasattr(input, "__len__"):
        for s in input:
            res *= transf_01_to_pm1(s)
    else:
        res = transf_01_to_pm1(input)

    return res

mmt_op = "M" # MR or M depending on whether the ancillas should be reset or not after readout

def add_CZ_err(pairs, noise):
    """Returns a quantum circuit with a CZ gate applied to the specified pair of qubits with added noise"""
    circuit = stim.Circuit()
    for pair in pairs:
        circuit.append_operation("CZ", pair)
        if noise > 0:
            circuit.append_operation("DEPOLARIZE2", pair, noise)
            # Special noise channel for Luca
            # circuit.append_operation("PAULI_CHANNEL_2", pair, noise*np.array([0,0,0,0,1,1,0,0,1,1,0,0,0,0,0]))
    circuit.append_operation("TICK")
    return circuit

def add_op_with_err(op:str, qbs:int, noise:float):
    """Returns a quantum circuit with the specified operation applied to the specified qubit and with added noise"""
    circuit = stim.Circuit()

    if hasattr(qbs, "__len__"):
        qubits_arr = qbs
    else:
        qubits_arr = [qbs]


    if noise > 0:
        if op == mmt_op: # For measurements, first depolarizing channel, then readout
            for qb in qubits_arr:
                circuit.append_operation("DEPOLARIZE1", qb, noise)
            for qb in qubits_arr:
                circuit.append_operation(op, qb)
        else: # For other operations, first operation, then depolarizing channel
            for qb in qubits_arr:
                circuit.append_operation(op, qb)
            for qb in qubits_arr:
                circuit.append_operation("DEPOLARIZE1", qb, noise)
    else:
        for qb in qubits_arr:
            circuit.append_operation(op, qb)

    return circuit


def S17_stab_mmts(circuit_only_for_nice_plotting, noise = 0, single_qb_noise:float=0, mmt_noise:float=0, do_dyn_decoupling:bool = False, round:int = 0, detectors:str = ""):
    """_summary_

    Args:
        circuit_only_for_nice_plotting (_type_): _description_
        noise (int, optional): _description_. Defaults to 0.
        do_dyn_decoupling (bool, optional): _description_. Defaults to False.
        round (int, optional): _description_. Defaults to 0.
        detectors (str, optional): If only X or only Z stabilizers should be included, this argument can be set to 'X' or 'Z'. Defaults to "".

    Returns:
        _type_: _description_
    """
    circuit = stim.Circuit()
    data_qbs = [qb_num("D" + str(i+1)) for i in range(9)]

    # ------------------
    #   Z stabilizers
    # ------------------
    circuit += add_op_with_err("SQRT_Y", [qb_num("Z1"), qb_num("Z2"), qb_num("Z3"), qb_num("Z4")], single_qb_noise)

    qb_pairs = [[qb_num("Z1"), qb_num("D1")], [qb_num("Z2"), qb_num("D5")], [qb_num("Z3"), qb_num("D3")]]
    circuit += add_CZ_err(qb_pairs, noise)

    qb_pairs = [[qb_num("Z1"), qb_num("D4")], [qb_num("Z2"), qb_num("D8")], [qb_num("Z3"), qb_num("D5")]]
    circuit += add_CZ_err(qb_pairs, noise)

    # Dynamical decoupling: Add Y gates to all data qubits
    if do_dyn_decoupling:
        circuit += add_op_with_err("Y", data_qbs, single_qb_noise)

    qb_pairs = [[qb_num("Z2"), qb_num("D4")], [qb_num("Z3"), qb_num("D2")], [qb_num("Z4"), qb_num("D6")]]
    circuit += add_CZ_err(qb_pairs, noise)

    qb_pairs = [[qb_num("Z2"), qb_num("D7")], [qb_num("Z3"), qb_num("D6")], [qb_num("Z4"), qb_num("D9")]]
    circuit += add_CZ_err(qb_pairs, noise)

    circuit.append_operation("TICK")

    # Change basis of ancilla qubits
    circuit += add_op_with_err("SQRT_Y_DAG", [qb_num("Z1"), qb_num("Z2"), qb_num("Z3"), qb_num("Z4")], single_qb_noise)

    circuit += add_op_with_err(mmt_op, [qb_num("Z1"), qb_num("Z3"), qb_num("Z2"), qb_num("Z4")], mmt_noise)

    circuit.append_operation("TICK")
    # ancilla_X = [qb_num("X" + str(i+1)) for i in range(4)]
    # ancilla_Z = [qb_num("Z" + str(i+1)) for i in range(4)]
    # circuit.append_operation("DEPOLARIZE1", ancilla_X, 0.1)
    # circuit.append_operation("DEPOLARIZE1", ancilla_Z, 0.1)

    if not(circuit_only_for_nice_plotting) and (detectors in ["", "Z"]):
        # round == 0: We don't do Z stabilizer measurements anyway
        if round == 1:
            for k in range(4):
                circuit.append_operation("DETECTOR", [stim.target_rec(-4+k)]) # Syndrome is just the measurement result of this round
        elif round > 1:
            for k in range(4):
                circuit.append_operation("DETECTOR", [stim.target_rec(-4+k), stim.target_rec(-4+k-16)]) # Syndrome is the product of this round's measurement result and the result from two rounds before



    # ------------------
    #   X stabilizers
    # ------------------
    circuit += add_op_with_err("SQRT_Y", [qb_num("X1"), qb_num("X2"), qb_num("X3"), qb_num("X4")], single_qb_noise)

    # Add SQRT_Y_DAG gates to all data qubits
    circuit += add_op_with_err("SQRT_Y_DAG", data_qbs, single_qb_noise)

    qb_pairs = [[qb_num("X2"), qb_num("D1")], [qb_num("X3"), qb_num("D5")], [qb_num("X4"), qb_num("D7")]]
    circuit += add_CZ_err(qb_pairs, noise)

    qb_pairs = [[qb_num("X2"), qb_num("D2")], [qb_num("X3"), qb_num("D6")], [qb_num("X4"), qb_num("D8")]]
    circuit += add_CZ_err(qb_pairs, noise)

    # Dynamical decoupling: Add Y gates to all data qubits
    if do_dyn_decoupling:
        circuit += add_op_with_err("Y", data_qbs, single_qb_noise)

    qb_pairs = [[qb_num("X1"), qb_num("D3")], [qb_num("X2"), qb_num("D5")], [qb_num("X3"), qb_num("D9")]]
    circuit += add_CZ_err(qb_pairs, noise)

    qb_pairs = [[qb_num("X1"), qb_num("D2")], [qb_num("X2"), qb_num("D4")], [qb_num("X3"), qb_num("D8")]]
    circuit += add_CZ_err(qb_pairs, noise)

    circuit += add_op_with_err("SQRT_Y_DAG", [qb_num("X1"), qb_num("X2"), qb_num("X3"), qb_num("X4")], single_qb_noise)

    # Add SQRT_Y_DAG gates to all data qubits
    circuit += add_op_with_err("SQRT_Y_DAG", data_qbs, single_qb_noise)

    circuit += add_op_with_err(mmt_op, [qb_num("X1"), qb_num("X3"), qb_num("X2"), qb_num("X4")], mmt_noise)

    if not(circuit_only_for_nice_plotting) and (detectors in ["", "X"]):
        # round == 0: Only do measurements but we cannot calculate a syndrome/detector yet
        if round == 1:
            for k in range(4):
                circuit.append_operation("DETECTOR", [stim.target_rec(-4+k)]) # Syndrome is this round's measurement result
        elif round > 1:
            for k in range(4):
                circuit.append_operation("DETECTOR", [stim.target_rec(-4+k), stim.target_rec(-4+k-16)]) # Syndrome is the product of this round's measurement result and the result from TWO rounds before

    circuit.append_operation("TICK")

    return circuit


ancilla_init_state = "1"

def get_S17_circuit(obs:str="Z", QEC_cycles:int=3, noise_CZ:float=0, single_qb_noise:float=0, mmt_noise:float=0, init_state:str = "0", do_dyn_decoupling:bool = False, detectors:str = ""):
    surf17_log_z = [qb_num("D1"), qb_num("D2"), qb_num("D3")]
    surf17_log_x = [qb_num("D1"), qb_num("D4"), qb_num("D7")]
    surf17_data_qbs = [qb_num("D" + str(i+1)) for i in range(9)]

    circuit_only_for_nice_plotting = False # Set to true if the circuit is not used for simulation but only for nice plotting
    c = stim.Circuit()

    # Initialize all qubits in 0
    for qb in range(17):
        c.append_operation("RZ", qb)
        
    # init ancilla in 1
    if ancilla_init_state == "1":
        for qb in [qb_num("X1"),qb_num("X2"),qb_num("X3"),qb_num("X4"), qb_num("Z1"), qb_num("Z2"), qb_num("Z3"), qb_num("Z4")]:
            c.append_operation("X", qb)

    # Prepare the logical surface-17 qubit in the specified state
    match init_state:
        case "0":
            pass
        case "1":
            for qb in surf17_log_z:
                c.append_operation("Z", qb)
        case "+":
            for qb in surf17_data_qbs:
                c.append_operation("SQRT_Y", qb)
        case _:
            raise ValueError(f"Initial state {init_state} is not supported.")

    c.append_operation("TICK")

    # Do a few rounds of error correction
    for i in range(QEC_cycles):
        c += S17_stab_mmts(circuit_only_for_nice_plotting,
                           noise=noise_CZ,
                           single_qb_noise=single_qb_noise,
                           mmt_noise=mmt_noise,
                           do_dyn_decoupling=do_dyn_decoupling,
                           round=i,
                           detectors=detectors,
                           )

    match(obs):
        case "Z":
            pass
        case "X":
            c.append_operation("SQRT_Y", surf17_log_x)
        case "Y":
            c.append_operation("SQRT_X_DAG", qb_num("D1"))
            c.append_operation("SQRT_Y", [qb_num("D4"), qb_num("D7")])
        case _:
            raise ValueError(f"Meausing the final observable {obs} is not supported.")

    c.append_operation("M", surf17_data_qbs)

    match(obs):
        case "Z":
            c.append_operation("OBSERVABLE_INCLUDE", [stim.target_rec(i) for i in [-7, -8, -9]], 0)
        case "X":
            c.append_operation("OBSERVABLE_INCLUDE", [stim.target_rec(i) for i in [-3, -6, -9]], 0)
        case "Y":
            c.append_operation("OBSERVABLE_INCLUDE", [stim.target_rec(i) for i in [-7, -8, -3, -6, -9]], 0)
        case _:
            raise ValueError("Meausing the final observable {obs} is not supported.")

    return c


def simulate_circuit_w_reset(
    circuit_in,
    p_amp_ge=0.01,
    ancilla_fb=False,
    after_clifford_depolarization_2=0.01,
    active_reset=True,
):
    ### Operator string.
    # qb: 1, 3, 5
    logical_qubits = [1, 3, 5]  # qubits of logical operator on S17
    operator_string = ""
    for i in range(circuit_in.num_qubits):
        if i in logical_qubits:
            operator_string += "X"
        else:
            operator_string += "I"

    # print(operator_string)

    simulator = stim.TableauSimulator()

    z_ancillas = [9, 13, 14, 18]
    x_ancillas = [2, 16, 11, 25]

    z_ancillas_dict = {i: {"msmt_last": None} for i in z_ancillas}
    x_ancillas_dict = {i: {"msmt_last": None} for i in x_ancillas}

    circuit = circuit_in.flattened()  # circuit.flattened()
    for k in range(len(circuit)):
        instruction = circuit[k]
        sub_circuit = circuit[k : k + 1]
        # print(instruction.name)
        if instruction.name == "MR":
            ts = instruction.targets_copy()
            z_ancillas_list = []
            x_ancillas_list = []
            qubits = [q.value for q in ts]

            for qubit in [q.value for q in ts]:
                # apply flip:
                flip_applied = False
                if ancilla_fb:
                    if qubit in z_ancillas:
                        # if z_ancillas_dict[qubit]['msmt_last'] == [True for i in range(2)]:
                        # if z_ancillas_dict[qubit]['msmt_last'] == [True]:
                        if z_ancillas_dict[qubit]["msmt_last"] in [
                            [True, True, True],
                            [False, True, True],
                            [True, False, True],
                        ]:  #
                            simulator.x(qubit)
                            flip_applied = True
                    if qubit in x_ancillas:
                        # if x_ancillas_dict[qubit]['msmt_last'] == [True for i in range(2)]:
                        if x_ancillas_dict[qubit]["msmt_last"] == [
                            [True, True, True],
                            [False, True, True],
                            [True, False, True],
                        ]:  # , [True, True, False]
                            # if x_ancillas_dict[qubit]['msmt_last'] == [True]:
                            simulator.x(qubit)
                            # print('flip_applied',qubit, simulator.peek_bloch(qubit))
                            flip_applied = True

                ## apply noise
                # check whether we are in |1>
                if simulator.peek_bloch(qubit) == stim.PauliString("-Z"):
                    if random.random() < p_amp_ge:
                        simulator.x(qubit)

                # we cannot change the measurement record... fake flip before msmt.
                # apply flip:
                if ancilla_fb and flip_applied:
                    simulator.x(qubit)
                    flip_applied = False

            simulator.do_circuit(sub_circuit)
            # correct msmt values if ancilla feedback
            if ancilla_fb:
                for index, qubit in enumerate([q.value for q in ts]):
                    msmt = simulator.current_measurement_record()[index - 8]
                    if qubit in z_ancillas:
                        if z_ancillas_dict[qubit]["msmt_last"] == None:
                            z_ancillas_dict[qubit]["msmt_last"] = [msmt]
                        else:
                            z_ancillas_dict[qubit]["msmt_last"].append(msmt)
                            # if len(z_ancillas_dict[qubit]['msmt_last']) > 2:
                            if len(z_ancillas_dict[qubit]["msmt_last"]) > 3:
                                z_ancillas_dict[qubit]["msmt_last"].pop(0)
                    elif qubit in x_ancillas:
                        if x_ancillas_dict[qubit]["msmt_last"] == None:
                            x_ancillas_dict[qubit]["msmt_last"] = [msmt]
                        else:
                            x_ancillas_dict[qubit]["msmt_last"].append(msmt)
                            # if len(x_ancillas_dict[qubit]['msmt_last']) > 2:
                            if len(x_ancillas_dict[qubit]["msmt_last"]) > 3:
                                x_ancillas_dict[qubit]["msmt_last"].pop(0)

        # 2-qubit gates different error probabilities.
        elif instruction.name == "DEPOLARIZE2":
            ts = instruction.targets_copy()
            qubits = [q.value for q in ts]
            for j in range(int(len(qubits) / 2)):
                simulator.depolarize2(
                    qubits[2 * j], qubits[2 * j + 1], p=after_clifford_depolarization_2
                )
            simulator.do_circuit(sub_circuit)
        else:
            simulator.do_circuit(sub_circuit)

    return simulator




def simulate_circuit(circuit_in, 
                     p_amp_ge = 0.01, 
                     ancilla_fb = False, 
                     after_clifford_depolarization_2 = 0.01,
                     count = 0): 

    simulator = stim.TableauSimulator()
    
    z_ancillas = [qb_num("Z1"), qb_num("Z2"), qb_num("Z3"), qb_num("Z4")]
    x_ancillas = [qb_num("X1"), qb_num("X2"), qb_num("X3"), qb_num("X4")]
    
    z_ancillas_dict = {i: {'msmt_last': None } for i in z_ancillas}
    x_ancillas_dict = {i: {'msmt_last': None } for i in x_ancillas}
    
    #Keep track of all states
    z_states_dict = {i: {'state': "START" } for i in z_ancillas}
    x_states_dict = {i: {'state': "START" } for i in x_ancillas}
    
    #Keep track of all next states
    z_next_dict = {i: {'next': "START" } for i in z_ancillas}
    x_next_dict = {i: {'next': "START" } for i in x_ancillas}
    
    z_toggle_dict = {i: {'toggle': False} for i in z_ancillas}
    x_toggle_dict = {i: {'toggle': False} for i in x_ancillas}
    
    circuit = circuit_in.flattened() #circuit.flattened()  
    
    for k in range(len(circuit)) :
        instruction = circuit[k]
        sub_circuit = circuit[k:k+1]
        #print(instruction.name)
        if  instruction.name == 'M' :
    
            ts = instruction.targets_copy()
    
            qubits = [q.value for q in ts]
            
            for qubit  in [q.value for q in  ts]:   
                # apply flip: 
                
                #Assign next state and if in TOGGLE state apply X-pulse
                if qubit in z_ancillas:
                    z_states_dict[qubit]['state'] = z_next_dict[qubit]['next']
                    #print(z_states_dict[qubit]['state'])
                    if z_states_dict[qubit]['state'] == "TOGGLE" and ancilla_fb:    
                        count = count + 1
                        simulator.x(qubit)
                        z_toggle_dict[qubit]['toggle'] = not z_toggle_dict[qubit]['toggle']
                       
                if qubit in x_ancillas:
                    x_states_dict[qubit]['state'] = x_next_dict[qubit]['next']
                     
                    if x_states_dict[qubit]['state'] == "TOGGLE" and ancilla_fb: 
                        simulator.x(qubit)
                        x_toggle_dict[qubit]['toggle'] = not x_toggle_dict[qubit]['toggle']

                ## apply noise    
                    # check whether we are in |1>
                if simulator.peek_bloch(qubit) == stim.PauliString("-Z"): 
                    if random.random() < p_amp_ge: 
                        simulator.x(qubit) 
                
                #Flip the state back so can erase the toggling effect
                if qubit in z_ancillas:
                    if z_toggle_dict[qubit]['toggle'] and ancilla_fb :   #and z_states_dict[qubit]['state'] == "TOGGLE"
                            simulator.x(qubit)
                    meas = simulator.measure(qubit)
                    if z_ancillas_dict[qubit]['msmt_last'] == None:
                        z_ancillas_dict[qubit]['msmt_last'] = [meas^z_toggle_dict[qubit]['toggle'] ]
                    else: 
                        z_ancillas_dict[qubit]['msmt_last'].append(meas^z_toggle_dict[qubit]['toggle'] )    # LB
                        if len(z_ancillas_dict[qubit]['msmt_last']) > 2:
                            z_ancillas_dict[qubit]['msmt_last'].pop(0) 
                    
                    if z_toggle_dict[qubit]['toggle'] and ancilla_fb : # and z_states_dict[qubit]['state'] == "TOGGLE"
                        simulator.x(qubit)
                                  
                if qubit in x_ancillas:
                    if x_toggle_dict[qubit]['toggle'] and ancilla_fb :  # and x_states_dict[qubit]['state'] == "TOGGLE"
                        simulator.x(qubit)
                    meas = simulator.measure(qubit)
                    if x_ancillas_dict[qubit]['msmt_last'] == None:
                        x_ancillas_dict[qubit]['msmt_last'] = [meas^x_toggle_dict[qubit]['toggle']]
                    else: 
                        
                        x_ancillas_dict[qubit]['msmt_last'].append(meas^x_toggle_dict[qubit]['toggle'])   ## LB
                        if len(x_ancillas_dict[qubit]['msmt_last']) > 2:
                            x_ancillas_dict[qubit]['msmt_last'].pop(0) 
                
                    if x_toggle_dict[qubit]['toggle'] and ancilla_fb :  # and x_states_dict[qubit]['state'] == "TOGGLE"
                        simulator.x(qubit)
                
                if qubit not in z_ancillas and  qubit not in x_ancillas:
                    simulator.measure(qubit)
                 
            #Idea: sep dict for toggled/not toggled msmts
            #state machine for all ancillas separately 
            for qubit in [q.value for q in ts]: 
                if qubit in z_ancillas:
                    if z_states_dict[qubit]['state'] == "TOGGLE":
                        
                        if z_ancillas_dict[qubit]['msmt_last'] == [0,0]:    
                            z_next_dict[qubit]['next'] = "TOGGLE"
                        elif z_ancillas_dict[qubit]['msmt_last'] == [0,1]:    
                            z_next_dict[qubit]['next'] = "TOGGLE"
                        elif z_ancillas_dict[qubit]['msmt_last'] == [1,0]:    
                            z_next_dict[qubit]['next'] = "NO TOGGLE"
                        else:
                            z_next_dict[qubit]['next'] = "NO TOGGLE"
                    
                    elif z_states_dict[qubit]['state'] == "NO TOGGLE":
                        if z_ancillas_dict[qubit]['msmt_last']  == [0,0]:    
                            z_next_dict[qubit]['next'] = "NO TOGGLE"
                        elif z_ancillas_dict[qubit]['msmt_last']  == [0,1]:    
                            z_next_dict[qubit]['next'] = "NO TOGGLE"
                        elif z_ancillas_dict[qubit]['msmt_last']  == [1,0]:    
                            z_next_dict[qubit]['next'] = "TOGGLE"
                        else:   
                            z_next_dict[qubit]['next'] = "TOGGLE"
                        
                    elif z_states_dict[qubit]['state'] == "START":
                       z_next_dict[qubit]['next'] = "NO TOGGLE"
                           
                if qubit in x_ancillas:
                    if x_states_dict[qubit]['state'] == "TOGGLE":
                        
                        if x_ancillas_dict[qubit]['msmt_last'] == [0,0]:    
                            x_next_dict[qubit]['next'] = "TOGGLE"
                        elif x_ancillas_dict[qubit]['msmt_last'] == [0,1]:    
                            x_next_dict[qubit]['next'] = "TOGGLE"
                        elif x_ancillas_dict[qubit]['msmt_last'] == [1,0]:    
                            x_next_dict[qubit]['next'] = "NO TOGGLE"
                        else:
                            x_next_dict[qubit]['next'] = "NO TOGGLE"
                    
                    elif x_states_dict[qubit]['state'] == "NO TOGGLE":
                        if x_ancillas_dict[qubit]['msmt_last']  == [0,0]:    
                            x_next_dict[qubit]['next'] = "NO TOGGLE"
                        elif x_ancillas_dict[qubit]['msmt_last']  == [0,1]:    
                            x_next_dict[qubit]['next'] = "NO TOGGLE"
                        elif x_ancillas_dict[qubit]['msmt_last']  == [1,0]:    
                            x_next_dict[qubit]['next'] = "TOGGLE"
                        else:   
                            x_next_dict[qubit]['next'] = "TOGGLE"
                        
                    elif x_states_dict[qubit]['state'] == "START":
                       x_next_dict[qubit]['next'] = "NO TOGGLE"
                
        elif instruction.name == 'DEPOLARIZE2' :
            ts = instruction.targets_copy()
            qubits = [q.value for q in ts]
            for j in range(int(len(qubits)/2)):
                simulator.depolarize2(qubits[2*j], qubits[2*j + 1], p = after_clifford_depolarization_2)
            simulator.do_circuit(sub_circuit)
        else: 
            simulator.do_circuit(sub_circuit)
    # print(count)
    return simulator
        

def count_logical_errors(
    circuit: stim.Circuit,
    num_shots: int,
    p_amp_ge=0.01,
    ancilla_fb=False,
    after_clifford_depolarization_2=0.01,
) -> int:
    # Sample the circuit.
    # sampler = circuit.compile_detector_sampler()
    # detection_events, observable_flips = sampler.sample(num_shots, separate_observables=True)

    # surface_code_circuit.detector_error_model(decompose_errors=True)
    measurements = []
    for i in range(num_shots):
        simulator = simulate_circuit(
            circuit,
            p_amp_ge=p_amp_ge,
            ancilla_fb=ancilla_fb,
            after_clifford_depolarization_2=after_clifford_depolarization_2,
        )
        measurements.append(simulator.current_measurement_record())
    measurements = np.array(measurements, dtype=np.bool_)
    # print('measurements',measurements)
    detection_events, observable_flips = circuit.compile_m2d_converter().convert(
        measurements=measurements, separate_observables=True, append_observables=False
    )  # ,

    # Configure a decoder using the circuit.
    detector_error_model = circuit.detector_error_model(decompose_errors=True)
    matcher = pymatching.Matching.from_detector_error_model(detector_error_model)

    # Run the decoder.
    predictions = matcher.decode_batch(detection_events)

    # Count the mistakes.
    num_errors = 0
    for shot in range(num_shots):
        actual_for_shot = observable_flips[shot]
        predicted_for_shot = predictions[shot]
        if not np.array_equal(actual_for_shot, predicted_for_shot):
            num_errors += 1
    return num_errors



def data_S17_rounds(
    decoding_rounds=3,
    after_clifford_depolarization=0.001,
    after_clifford_depolarization_2=0.01,
    after_reset_flip_probability=0.001,
    before_measure_flip_probability=0.001,
    before_round_data_depolarization=0.01,
    p_ge=0.5,
    runs=1000,
    ancilla_fb=False,
    obs="Z", 
    init_state="0"
):
    logical_err_array = []

    do_dyn_decoupling = False
    #rounds_array = range(3, decoding_rounds, 3)
    rounds_array = list([1,2,3,4,5,6,8,10,12,16])
    for rounds in rounds_array:
        surface_code_circuit = get_S17_circuit(obs=obs, QEC_cycles=rounds, noise_CZ=after_clifford_depolarization_2, 
                                               detectors="", do_dyn_decoupling=do_dyn_decoupling, init_state=init_state,
                                               single_qb_noise=after_clifford_depolarization, mmt_noise=before_measure_flip_probability)

        n_errors = count_logical_errors(
            surface_code_circuit,
            runs,
            p_amp_ge=p_ge,
            ancilla_fb=ancilla_fb,
            after_clifford_depolarization_2=after_clifford_depolarization_2,
        )
        logical_err = n_errors / runs
        logical_err_array.append(logical_err)
    return rounds_array, logical_err_array



def plot_data_S17_rounds(
    decoding_rounds=22,
    after_clifford_depolarization_1=0.001,
    after_clifford_depolarization_2=0.01,
    after_reset_flip_probability=0.00,
    before_round_data_depolarization=0.00,
    before_measure_flip_probability=0.00,
    p_ge=0.01,
    runs=100,
    plot_from_data_dict=False,
    data_dict=None,
):
    if not plot_from_data_dict:
        data_dict = {}

    # No amplitude damping, no ancilla feedback
    if not plot_from_data_dict:
        rounds_array, logical_err_array = data_S17_rounds(
            decoding_rounds=decoding_rounds,
            after_clifford_depolarization=after_clifford_depolarization_1,
            after_clifford_depolarization_2=after_clifford_depolarization_2,
            after_reset_flip_probability=after_reset_flip_probability,
            before_measure_flip_probability=before_measure_flip_probability,
            before_round_data_depolarization=before_round_data_depolarization,
            p_ge=0,
            runs=runs,
            ancilla_fb=False,
            obs="Z", 
            init_state="0"
        )
        data_dict["no_amp_no_fb"] = {}
        data_dict["no_amp_no_fb"]["rounds_array"] = list(rounds_array)
        data_dict["no_amp_no_fb"]["logical_err_array"] = logical_err_array
    else:
        rounds_array = data_dict["no_amp_no_fb"]["rounds_array"]
        logical_err_array = data_dict["no_amp_no_fb"]["logical_err_array"]

    param, vars = scipy.optimize.curve_fit(
        lambda t, b: 0.5-0.5*np.exp(-b*t), rounds_array, logical_err_array
    )
    T1 =  round(1/param[0],1)
    epsilon_L = round(0.5*(1-np.exp(-1/T1)),4)
    plt.plot(
        rounds_array,
        logical_err_array,
        #label=rf"raw: $p_{{ge}}:${0}",
        linestyle="None",
        color="r",
        marker="o",
    )
    plt.plot(
        rounds_array,
        0.5-0.5*np.exp(-param[0]*np.array(rounds_array)),
        label=rf"fit: $ p_{{ge}}:${0} ($\mathbf{{\epsilon_L: {epsilon_L}}}$)",
        linestyle="solid",
        color="r",
        #marker="o",
        mfc='none'
    )
    if not plot_from_data_dict:
        data_dict["no_amp_no_fb"]["T1"] = T1
        data_dict["no_amp_no_fb"]["epsilon_L"] = epsilon_L
        
    # With amplitude damping, with ancilla feedback
    if not plot_from_data_dict:
        rounds_array, logical_err_array = data_S17_rounds(
            decoding_rounds=decoding_rounds,
            after_clifford_depolarization=after_clifford_depolarization_1,
            after_clifford_depolarization_2=after_clifford_depolarization_2,
            after_reset_flip_probability=after_reset_flip_probability,
            before_measure_flip_probability=before_measure_flip_probability,
            before_round_data_depolarization=before_round_data_depolarization,
            p_ge=p_ge,
            runs=runs,
            ancilla_fb=True,
            obs="Z", 
            init_state="0"
        )
        data_dict["with_amp_with_fb"] = {}
        data_dict["with_amp_with_fb"]["rounds_array"] = list(rounds_array)
        data_dict["with_amp_with_fb"]["logical_err_array"] = logical_err_array
    else:
        rounds_array = data_dict["with_amp_with_fb"]["rounds_array"]
        logical_err_array = data_dict["with_amp_with_fb"]["logical_err_array"]

    param, vars = scipy.optimize.curve_fit(
        lambda t, b: 0.5-0.5*np.exp(-b*t), rounds_array, logical_err_array
    )
    T1 =  round(1/param[0],1)
    epsilon_L = round(0.5*(1-np.exp(-1/T1)),4)
    plt.plot(
        rounds_array,
        logical_err_array,
        #label=rf"raw: fb, $p_{{ge}}:${p_ge}",
        linestyle="None",
        color="g",
        marker="o",
    )
    plt.plot(
        rounds_array,
        0.5-0.5*np.exp(-param[0]*np.array(rounds_array)),
        label=rf"fit: fb, $p_{{ge}}:${p_ge} ($\mathbf{{\epsilon_L: {epsilon_L}}}$)",
        linestyle="solid",
        color="g",
        #marker="o",
        mfc='none'
    )
    if not plot_from_data_dict:
        data_dict["with_amp_with_fb"]["T1"] = T1
        data_dict["with_amp_with_fb"]["epsilon_L"] = epsilon_L

    # With amplitude damping, no ancilla feedback
    if not plot_from_data_dict:
        rounds_array, logical_err_array = data_S17_rounds(
            decoding_rounds=decoding_rounds,
            after_clifford_depolarization=after_clifford_depolarization_1,
            after_clifford_depolarization_2=after_clifford_depolarization_2,
            after_reset_flip_probability=after_reset_flip_probability,
            before_measure_flip_probability=before_measure_flip_probability,
            before_round_data_depolarization=before_round_data_depolarization,
            p_ge=p_ge,
            runs=runs,
            ancilla_fb=False,
            obs="Z", 
            init_state="0"
        )
        data_dict["with_amp_no_fb"] = {}
        data_dict["with_amp_no_fb"]["rounds_array"] = list(rounds_array)
        data_dict["with_amp_no_fb"]["logical_err_array"] = logical_err_array
    else:
        rounds_array = data_dict["with_amp_no_fb"]["rounds_array"]
        logical_err_array = data_dict["with_amp_no_fb"]["logical_err_array"]

    param, vars = scipy.optimize.curve_fit(
        lambda t, b: 0.5-0.5*np.exp(-b*t), rounds_array, logical_err_array
    )
    T1 =  round(1/param[0],1)
    epsilon_L = round(0.5*(1-np.exp(-1/T1)),4)
    plt.plot(
        rounds_array,
        logical_err_array,
        #label=rf"raw: no fb, $p_{{ge}}:${p_ge}",
        linestyle="None",
        color="b",
        marker="o",
    )
    plt.plot(
        rounds_array,
        0.5-0.5*np.exp(-param[0]*np.array(rounds_array)),
        label=rf"fit: no fb, $p_{{ge}}:${p_ge} ($\mathbf{{\epsilon_L: {epsilon_L}}}$)",
        linestyle="solid",
        color="b",
        #marker="o",
        mfc='none'
    )
    if not plot_from_data_dict:
        data_dict["with_amp_no_fb"]["T1"] = T1
        data_dict["with_amp_no_fb"]["epsilon_L"] = epsilon_L


    # Store data:
    print("data_dict", data_dict)
    if not plot_from_data_dict:
        with open(
            f"data/S17/S17_rounds{decoding_rounds-1}_peg{p_ge}_polerr{after_clifford_depolarization_2}_rep{runs}_ancinit{ancilla_init_state}.txt",
            "w",
        ) as f:
            json.dump(data_dict, f, indent=4)
            print(
                f"wrote data to file data/S17/S17_rounds{decoding_rounds-1}_peg{p_ge}_polerr{after_clifford_depolarization_2}_rep{runs}_ancinit{ancilla_init_state}.txt"
            )

    plt.legend(loc="lower right", ncols= 1)
    plt.title(
        rf" S17, $E_L$, $|0\rangle_L$"
    )
    plt.xlabel(rf"Number of QEC cycles $n$")
    plt.ylabel("logical error probability $E_L$ ")
    plt.ylim([0, 0.5])
    plt.savefig(
        f"data/S17/S17_rounds{decoding_rounds-1}_peg{p_ge}_polerr{after_clifford_depolarization_2}_rep{runs}_ancinit{ancilla_init_state}.png"
    )
    print(
        f"saved plot to data/S17/S17_rounds{decoding_rounds-1}_peg{p_ge}_polerr{after_clifford_depolarization_2}_rep{runs}_ancinit{ancilla_init_state}.png"
    )
    plt.show()


