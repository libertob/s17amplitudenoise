import stim
import collections
import math
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import collections
import scipy
import numpy as np
import json


# Different state machine with states {0t, 1t, 0nt, 1nt}


def w2_stab_no_reset(
    p_ge=0.01,
    cycles=10,
    parity=0,
    runs=1000,
    after_reset_flip_probability=0.001,
    after_clifford_depolarization_1=0.001,
    after_clifford_depolarization_2=0.01,
    before_measure_flip_probability=0.001,
    ancilla_fb=False,
    stabilizer_type="Z_Z",
):
    errors = 0
    ancilla_idx = 1
    data_0_idx = 0
    data_1_idx = 2
    data_idx = [data_0_idx, data_1_idx]
    qubits_idx = [data_0_idx, data_1_idx, ancilla_idx]
    two_qubit_gates_idx = [data_0_idx, ancilla_idx, data_1_idx, ancilla_idx]

    for i in range(runs):
        sim = stim.TableauSimulator()

        # FIFO queue of measurments
        parity_meas = collections.deque(maxlen=2)

        # Initialisation circuit
        sim.reset(*qubits_idx)
        sim.x_error(*qubits_idx, p=after_reset_flip_probability)

        # Prepare mixed state
        sim.h(data_0_idx)
        sim.h(data_1_idx)
        sim.depolarize1(data_0_idx, p=after_clifford_depolarization_1)
        sim.depolarize1(data_1_idx, p=after_clifford_depolarization_1)

        if stabilizer_type == "X_X":
            sim.h(data_0_idx)
            sim.h(data_1_idx)
            sim.depolarize1(data_0_idx, p=after_clifford_depolarization_1)
            sim.depolarize1(data_1_idx, p=after_clifford_depolarization_1)
        # ZZ, no ancilla feedback
        sim.cnot(data_0_idx, ancilla_idx)
        sim.depolarize2(
            data_0_idx, ancilla_idx, p=after_clifford_depolarization_2
        )
        sim.cnot(data_1_idx, ancilla_idx)
        sim.depolarize2(
            data_1_idx, ancilla_idx, p=after_clifford_depolarization_2
        )

        if stabilizer_type == "X_X":
            sim.h(data_0_idx)
            sim.h(data_1_idx)
            sim.depolarize1(data_0_idx, p=after_clifford_depolarization_1)
            sim.depolarize1(data_1_idx, p=after_clifford_depolarization_1)

        anc = sim.peek_bloch(ancilla_idx)
        if anc == stim.PauliString("-Z"):
            sim.x_error(ancilla_idx, p=p_ge)

        # Symmetric Readout error
        sim.x_error(ancilla_idx, p=before_measure_flip_probability)
        parity_meas.append(sim.measure(ancilla_idx))

        state = "0nt"
        nextstate = "0nt"
        # cycles with feedback only on the ancilla qb, not on data qbs
        for j in range(cycles):
            state = nextstate

            if stabilizer_type == "X_X":
                sim.h(data_0_idx)
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p=after_clifford_depolarization_1)
                sim.depolarize1(data_1_idx, p=after_clifford_depolarization_1)
            # ZZ
            sim.cnot(data_0_idx, ancilla_idx)
            sim.depolarize2(
                data_0_idx, ancilla_idx, p=after_clifford_depolarization_2
            )
            sim.cnot(data_1_idx, ancilla_idx)
            sim.depolarize2(
                data_1_idx, ancilla_idx, p=after_clifford_depolarization_2
            )

            if stabilizer_type == "X_X":
                sim.h(data_0_idx)
                sim.h(data_1_idx)
                sim.depolarize1(data_0_idx, p=after_clifford_depolarization_1)
                sim.depolarize1(data_1_idx, p=after_clifford_depolarization_1)

            if state == "1t" and ancilla_fb:
                sim.x(ancilla_idx)

            if state == "0t" and ancilla_fb:
                sim.x(ancilla_idx)

            # Apply T1 decay during ro on ancilla
            anc = sim.peek_bloch(ancilla_idx)
            if anc == stim.PauliString("-Z"):
                sim.x_error(ancilla_idx, p=p_ge)

            meas = sim.measure(ancilla_idx)
            parity_meas.append(meas)

            # State machine
            if state == "1t":
                nextstate = "0nt" if meas == 0 else "1nt"
            if state == "0t":
                nextstate = "0t" if meas == 0 else "1t"
            if state == "1nt":
                nextstate = "0t" if meas == 0 else "1t"
            if state == "0nt":
                nextstate = "0nt" if meas == 0 else "1nt"

            # If we performed ancilla fb we need to invert our msmt result

        # Correct stabilizer parity in last round.
        correct_data = False
        if ancilla_fb:
            if state in ["1t", "0t"] and (not parity_meas[1]) ^ parity_meas[0] == (
                not parity
            ):
                correct_data = True

            elif state in ["1nt", "0nt"] and parity_meas[1] ^ parity_meas[0] == (
                not parity
            ):
                correct_data = True
        else:
            correct_data = parity_meas[1] ^ parity_meas[0] == (not parity)

        if correct_data:
            if stabilizer_type == "Z_Z":
                sim.x(data_1_idx)
            elif stabilizer_type == "X_X":
                sim.z(data_1_idx)

        # if sim.measure_observable(stim.PauliString(stabilizer_type)) != parity:
        #     errors += 1
        
        # Adapted to measure observable <ZZ>
        if sim.measure_observable(stim.PauliString(stabilizer_type)):
            errors += -1
        else: 
            errors += 1
        parity_meas.clear()

    return errors


def data_w2_rounds(
        parity=0,
        decoding_rounds=10,
        after_clifford_depolarization_1=0.001,
        after_clifford_depolarization_2=0.01,
        after_reset_flip_probability=0.001,
        before_measure_flip_probability=0.001,
        p_ge=0.01,
        runs=100,
        ancilla_fb=False,
        stabilizer_type="Z_Z",
):
    logical_err_array = []

    rounds_array = range(1, decoding_rounds, 1)
    for rounds in rounds_array:

        n_errors = w2_stab_no_reset(
                    p_ge=p_ge,
                    cycles=rounds,
                    parity=parity,
                    runs=runs,
                    after_reset_flip_probability=after_reset_flip_probability,
                    after_clifford_depolarization_1=after_clifford_depolarization_1,
                    after_clifford_depolarization_2=after_clifford_depolarization_2,
                    before_measure_flip_probability=before_measure_flip_probability,
                    ancilla_fb=ancilla_fb,
                    stabilizer_type=stabilizer_type,
        )
        logical_err = n_errors / runs
        logical_err_array.append(logical_err)
    return rounds_array, logical_err_array


def plot_data_w2_rounds(
    parity=1,
    decoding_rounds=10,
    after_clifford_depolarization_1=0.001,
    after_clifford_depolarization_2=0.01,
    after_reset_flip_probability=0.001,
    before_measure_flip_probability=0.001,
    p_ge=0.01,
    runs=1000,
    plot_from_data_dict = False, 
    data_dict = {}, 
    stabilizer_type="Z_Z",
):
    # start steady state fit after 3 cycles. 
    t_start = 3
    parity_str = 'ZZ' if stabilizer_type=='Z_Z' else 'XX'
    
    if not plot_from_data_dict:
        data_dict = {}
        
    # No amplitude damping, no ancilla feedback
    if not plot_from_data_dict:    
        rounds_array, logical_err_array  = data_w2_rounds(
                        parity=parity,
                        decoding_rounds=decoding_rounds,
                        after_clifford_depolarization_1=after_clifford_depolarization_1,
                        after_clifford_depolarization_2=after_clifford_depolarization_2,
                        after_reset_flip_probability=after_reset_flip_probability,
                        before_measure_flip_probability=before_measure_flip_probability,
                        p_ge=0,
                        runs=runs,
                        ancilla_fb=False,
                        stabilizer_type=stabilizer_type,
        )
        data_dict["no_amp_no_fb"] = {}
        data_dict["no_amp_no_fb"]["rounds_array"] = list(rounds_array)
        data_dict["no_amp_no_fb"]["logical_err_array"] = logical_err_array
    else: 
        rounds_array = data_dict["no_amp_no_fb"]["rounds_array"]
        logical_err_array = data_dict["no_amp_no_fb"]["logical_err_array"]

    param, vars = scipy.optimize.curve_fit(
        lambda t, b: b, rounds_array[t_start:], logical_err_array[t_start:]
    )
    plt.plot(
        rounds_array,
        logical_err_array,
        label=rf"raw: $p_{{ge}}:${0}",
        linestyle="None",
        color="r",
        marker="o",
    )
    plt.plot(
        rounds_array,
        [param[0] for i in range(len(rounds_array))],
        label=rf"fit: $ p_{{ge}}:${0} $(\mathbf{{\langle {parity_str} \rangle_{{mean}}:{ round(param[0],3)}}})$",
        linestyle="dashed",
        color="r",
        mfc='none'
    )
        
        
    # With amplitude damping, with ancilla feedback
    if not plot_from_data_dict:
        rounds_array, logical_err_array  = data_w2_rounds(
                        parity=parity,
                        decoding_rounds=decoding_rounds,
                        after_clifford_depolarization_1=after_clifford_depolarization_1,
                        after_clifford_depolarization_2=after_clifford_depolarization_2,
                        after_reset_flip_probability=after_reset_flip_probability,
                        before_measure_flip_probability=before_measure_flip_probability,
                        p_ge=p_ge,
                        runs=runs,
                        ancilla_fb=True,
                        stabilizer_type=stabilizer_type,
        )
        data_dict["with_amp_with_fb"] = {}
        data_dict["with_amp_with_fb"]["rounds_array"] = list(rounds_array)
        data_dict["with_amp_with_fb"]["logical_err_array"] = logical_err_array
    else:
        rounds_array = data_dict["with_amp_with_fb"]["rounds_array"]
        logical_err_array = data_dict["with_amp_with_fb"]["logical_err_array"]

    param, vars = scipy.optimize.curve_fit(
        lambda t, b: b, rounds_array[t_start:], logical_err_array[t_start:]
    )
    plt.plot(
        rounds_array,
        logical_err_array,
        label=rf"raw: fb, $p_{{ge}}:${p_ge}",
        linestyle="None",
        color="g",
        marker="o",
    )
    plt.plot(
        rounds_array,
        [param[0] for i in range(len(rounds_array))],
        label=rf"fit: fb, $p_{{ge}}:${p_ge} $(\mathbf{{\langle {parity_str} \rangle_{{mean}}:{ round(param[0],3)}}})$",
        linestyle="dashed",
        color="g",
        mfc='none'
    )

        
    # With amplitude damping, no ancilla feedback
    if not plot_from_data_dict:
        
        rounds_array, logical_err_array  = data_w2_rounds(
                        parity=parity,
                        decoding_rounds=decoding_rounds,
                        after_clifford_depolarization_1=after_clifford_depolarization_1,
                        after_clifford_depolarization_2=after_clifford_depolarization_2,
                        after_reset_flip_probability=after_reset_flip_probability,
                        before_measure_flip_probability=before_measure_flip_probability,
                        p_ge=p_ge,
                        runs=runs,
                        ancilla_fb=False,
                        stabilizer_type=stabilizer_type,
        )
        data_dict["with_amp_no_fb"] = {}
        data_dict["with_amp_no_fb"]["rounds_array"] = list(rounds_array)
        data_dict["with_amp_no_fb"]["logical_err_array"] = logical_err_array
    else:
        rounds_array = data_dict["with_amp_no_fb"]["rounds_array"]
        logical_err_array = data_dict["with_amp_no_fb"]["logical_err_array"]

    param, vars = scipy.optimize.curve_fit(
        lambda t, b: b, rounds_array[t_start:], logical_err_array[t_start:]
    )
    plt.plot(
        rounds_array,
        logical_err_array,
        label=rf"raw: $p_{{ge}}:${p_ge}",
        linestyle="None",
        color="b",
        marker="o",
    )
    plt.plot(
        rounds_array,
        [param[0] for i in range(len(rounds_array))],
        label=rf"fit: no fb $p_{{ge}}:${p_ge} $(\mathbf{{\langle {parity_str} \rangle_{{mean}}:{ round(param[0],3)}}})$",
        linestyle="dashed",
        color="b",
        mfc='none'
    )



    # Store data:
    print("data_dict", data_dict)

    
    if not plot_from_data_dict:
        with open(
            f"data/w2/w2_rounds{decoding_rounds-1}_peg{p_ge}_p2q{after_clifford_depolarization_2}_rep{runs}.txt",
            "w",
        ) as f:
            json.dump(data_dict, f, indent=4)
            print(
                f"wrote data to file data/w2/w2_rounds{decoding_rounds-1}_peg{p_ge}_p2q{after_clifford_depolarization_2}_rep{runs}.txt"
            )

    if parity == 0: 
        plt.legend(loc="lower left", borderaxespad=0)   
        target_state_str = '+1'
    else: 
        plt.legend(loc="upper left", borderaxespad=0)   
        target_state_str = '-1'
    plt.title(
        rf" $\langle {parity_str}\rangle$ for preparation of ${parity_str} = {target_state_str} $"
    )
    plt.xlabel("# stabilization cycles, N")
    plt.ylabel(rf"Pauli op. value $\langle {parity_str} \rangle$")
    
    if parity == 0: 
        plt.ylim([0.5, 1.0])
    else: 
        plt.ylim([-1.0, -0.5])
    plt.savefig(
        f"data/w2/w2_rounds{decoding_rounds-1}_peg{p_ge}_p2q{after_clifford_depolarization_2}_rep{runs}.png"
    )
    print(
        f"saved plot to data/w2/w2_rounds{decoding_rounds-1}_peg{p_ge}_p2q{after_clifford_depolarization_2}_rep{runs}.png"
    )
    plt.show()

